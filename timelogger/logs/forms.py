from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator

from . import models

class DateInput(forms.DateInput):
    input_type = 'date'


class CreateLog(forms.ModelForm):

    duration = forms.DecimalField(widget=forms.NumberInput(attrs={'min':0.01, 'max':99}), max_digits=4, decimal_places=2, validators=[MinValueValidator(0.01)])

    class Meta:
        model = models.Log
        fields = ['project', 'duration']
        widgets = {
            'day': DateInput(),
        }
