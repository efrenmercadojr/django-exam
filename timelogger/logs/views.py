import datetime

from django.shortcuts import render, redirect
from django.utils import timezone
from django.views import generic
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Sum

from . import forms
from .models import Project, Log


@login_required(login_url="/accounts/login/")
def index(request):
    if request.method == 'GET':
        now = datetime.datetime.now()
        return redirect(reverse('logs:logs_view', args=(now.year, now.month, now.day,)))
    else:
        #change day
        new_day = datetime.datetime.strptime(request.POST.get('day'), '%Y-%m-%d').date()
        return redirect(reverse('logs:logs_view', args=(new_day.year, new_day.month, new_day.day,)))



@login_required(login_url="/accounts/login/")
def logs_view(request, year, month, day):

    form = forms.CreateLog()

    selected_day = datetime.date(int(year), int(month), int(day))

    logs = Log.objects.filter(
        day__year = selected_day.year,
        day__month = selected_day.month,
        day__day = selected_day.day,
        user = request.user,
    ).order_by('-logtime')

    day_sum = logs.aggregate(Sum('duration'))


    month_logs = Log.objects.filter(
        day__year = selected_day.year,
        day__month = selected_day.month,
        user = request.user,
    )

    month_sum = month_logs.aggregate(Sum('duration'))


    week_logs = Log.objects.filter(
        day__week = selected_day.isocalendar()[1],
    )

    week_sum = week_logs.aggregate(Sum('duration'))

    context = {
        'createlogform': form,
        'date': selected_day,
        'logs': logs,
        'day_sum': day_sum,
        'month_sum': month_sum,
        'week_sum': week_sum,
    }



    return render(request, 'logs/index.html', context)


@login_required(login_url="/accounts/login/")
def log_create(request, year, month, day):
    if request.method == 'POST':
        form = forms.CreateLog(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.logtime = timezone.now()
            instance.day = datetime.date(int(year), int(month), int(day))
            instance.save()
            return redirect(reverse('logs:logs_view', args=(year, month, day,)))


@login_required(login_url="/accounts/login/")
def log_delete(request, year, month, day):
    if request.method == 'POST':
        log_id = request.POST['id']
        log = Log.objects.filter(id=log_id)
        log.delete()
        return redirect(reverse('logs:logs_view', args=(year, month, day,)))