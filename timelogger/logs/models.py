import datetime
from decimal import Decimal

from django.db import models
from django.utils import timezone
from django.db.models import Sum
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator


class Project(models.Model):

    title = models.CharField(max_length=100)
    description = models.CharField(max_length=300)

    def __str__(self):
        return self.title

    def get_total_hours(self):
        logs = Log.objects.filter(project=self)
        total = logs.aggregate(Sum('duration'))
        return total['duration__sum']


class Log(models.Model):

    project = models.ForeignKey(
        Project, 
        on_delete=models.SET_NULL, 
        blank=True, null=True
    )
    day = models.DateField('day')
    duration = models.DecimalField(max_digits=4, decimal_places=2, validators=[MinValueValidator(Decimal(0.01))])
    logtime = models.DateTimeField('logtime')
    user = models.ForeignKey(User, default=None, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.day)
    
    def is_late(self):
        return self.logtime.date() > self.day