from django.contrib import admin

from .models import Project, Log


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'get_total_hours')

admin.site.register(Project, ProjectAdmin)
admin.site.register(Log)