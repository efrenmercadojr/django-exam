from django.conf.urls import url, include
from django.contrib import admin

from . import views


app_name = 'logs'
urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})$', views.logs_view, name="logs_view"),
    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/create$', views.log_create, name="create"),
    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/delete$', views.log_delete, name="delete"),
]
