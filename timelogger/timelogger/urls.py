from django.conf.urls import url, include
from django.contrib import admin

from . import views


urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^accounts/',include('allauth.urls')),
    #url(r'^accounts/', include('accounts.urls')),
    url(r'^logs/', include('logs.urls')),
    url(r'^admin/', admin.site.urls),
]
